

function toggleNav() {
  const toggleNav = document.querySelector(".toggle");
  const sideNav = document.querySelector("section");
  toggleNav.classList.toggle("active");
  sideNav.classList.toggle("active");
}
